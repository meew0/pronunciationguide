package meew0.pguide.lexconvert;

import meew0.pguide.PronunciationGuide;
import meew0.pguide.lib.Term;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

import java.util.Collection;

/**
 * Created by meew0 on 04/08/18.
 */
public class CacheRebuildRunner extends TaskRunner {

    @Override
    public void internalRun() {
        if(!PronunciationGuide.lexconvertCache.canInvoke()) {
            Minecraft.getMinecraft().player.sendMessage(
                    new TextComponentTranslation("pguide.adv.cannotrebuild").setStyle(
                            new Style().setColor(TextFormatting.RED)));
            return;
        }

        Collection<Term> terms = PronunciationGuide.TERM_REGISTRY.getAllTerms();

        Minecraft.getMinecraft().player.sendMessage(
                new TextComponentTranslation("pguide.adv.rebuild_start", terms.size()));

        int i = 0;
        for(Term term : terms) {
            i += 1;

            for(String alt : term.getAlternativesUnlocalised()) {
                String ipa = I18n.format(alt);
                PronunciationGuide.lexconvertCache.forceRebuild(alt, ipa, term.getId());
            }

            Minecraft.getMinecraft().player.sendStatusMessage(
                    new TextComponentTranslation("pguide.adv.taskprogress", i, terms.size())
                            .setStyle(new Style().setColor(TextFormatting.DARK_GREEN)),
                    true);
        }

        Minecraft.getMinecraft().player.sendMessage(
                new TextComponentTranslation("pguide.adv.rebuild_end", i, terms.size()));
    }
}
