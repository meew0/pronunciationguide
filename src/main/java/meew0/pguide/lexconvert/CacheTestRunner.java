package meew0.pguide.lexconvert;

import com.mojang.text2speech.Narrator;
import meew0.pguide.PronunciationGuide;
import meew0.pguide.lib.Term;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

/**
 * Created by meew0 on 04/08/18.
 */
public class CacheTestRunner extends TaskRunner {
    @Override
    public void internalRun() {
        for(CacheEntry entry : PronunciationGuide.lexconvertCache.entries()) {
            Term term = PronunciationGuide.TERM_REGISTRY.getTermById(entry.getTermId());
            String narratorKey = PronunciationGuide.lexconvertCache.get(entry.getKey());
            if(narratorKey != null) {
                Minecraft.getMinecraft().player.sendStatusMessage(
                        new TextComponentTranslation("pguide.adv.pronouncing", term.getLocalised())
                                .setStyle(new Style().setColor(TextFormatting.DARK_GREEN)),
                        true);
                PronunciationGuide.narrator.say(narratorKey);
            } else {
                Minecraft.getMinecraft().player.sendStatusMessage(
                        new TextComponentTranslation("pguide.adv.narratorkeyunavailable", term.getLocalised())
                                .setStyle(new Style().setColor(TextFormatting.DARK_RED)),
                        true);
            }

            sleep(2000);
        }
    }
}
