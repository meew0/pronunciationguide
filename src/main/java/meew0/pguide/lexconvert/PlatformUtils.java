package meew0.pguide.lexconvert;

import com.mojang.text2speech.Narrator;
import com.mojang.text2speech.NarratorLinux;
import com.mojang.text2speech.NarratorOSX;
import com.mojang.text2speech.NarratorWindows;
import meew0.pguide.PronunciationGuide;

/**
 * Created by meew0 on 04/08/18.
 */
public class PlatformUtils {
    public enum Platform {
        WINDOWS, MAC, LINUX
    }

    public static <T> T resolvePlatformSpecific(Platform platform, T windows, T mac, T linux) {
        switch(platform) {
            case WINDOWS: return windows;
            case MAC: return mac;
            case LINUX: return linux;
            default: return null;
        }
    }

    public static <T> T resolvePlatformSpecific(T windows, T mac, T linux) {
        return resolvePlatformSpecific(getCurrentPlatform(), windows, mac, linux);
    }

    public static Platform getCurrentPlatform() {
        Narrator narrator = PronunciationGuide.narrator;
        if(narrator instanceof NarratorWindows) return Platform.WINDOWS;
        if(narrator instanceof NarratorOSX) return Platform.MAC;
        if(narrator instanceof NarratorLinux) return Platform.LINUX;

        return null;
    }
}
