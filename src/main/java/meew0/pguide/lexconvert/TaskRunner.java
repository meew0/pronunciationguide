package meew0.pguide.lexconvert;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;

/**
 * Created by meew0 on 04/08/18.
 */
public abstract class TaskRunner implements Runnable {
    private boolean isRunning = false;

    @Override
    public void run() {
        if(isRunning) {
            Minecraft.getMinecraft().player.sendMessage(
                    new TextComponentTranslation("pguide.adv.taskalreadyrunning")
                            .setStyle(new Style().setColor(TextFormatting.RED)));
        }

        isRunning = true;
        internalRun();
        isRunning = false;
    }

    public void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public abstract void internalRun();
}
