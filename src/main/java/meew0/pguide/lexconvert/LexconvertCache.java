package meew0.pguide.lexconvert;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import meew0.pguide.PronunciationGuide;

import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Created by meew0 on 04/08/18.
 */
public class LexconvertCache {
    public static String pythonExecutablePath, lexconvertPath;
    public static String windowsFormat, macFormat, linuxFormat;

    private final File file;
    private final HashMap<String, CacheEntry> entries;
    private final Gson gson;

    private static final String IPA_FORMAT = "unicode-ipa";

    public LexconvertCache(File file) {
        this.file = file;

        this.gson = new GsonBuilder().create();
        this.entries = new HashMap<>();

        load();
    }

    @Nullable
    public String get(String key, String ipa, String termId) {
        return get(key, ipa, termId, PlatformUtils.getCurrentPlatform());
    }

    @Nullable
    public String get(String key, String ipa, String termId, PlatformUtils.Platform platform) {
        if(entries.containsKey(key)) {
            return entries.get(key).getFormat(platform);
        } else if(canInvoke()) {
            CacheEntry entry = forceRebuild(key, ipa, termId);
            return entry.getFormat(platform);
        } else {
            return null;
        }
    }

    public CacheEntry forceRebuild(String key, String ipa, String termId) {
        CacheEntry entry = buildEntry(key, ipa, termId);
        entries.put(key, entry);
        return entry;
    }

    @Nullable
    public String get(String key) {
        return get(key, PlatformUtils.getCurrentPlatform());
    }

    @Nullable
    public String get(String key, PlatformUtils.Platform platform) {
        if(entries.containsKey(key)) {
            return entries.get(key).getFormat(platform);
        } else {
            return null;
        }
    }

    public Collection<CacheEntry> entries() {
        return entries.values();
    }

    private CacheEntry buildEntry(String key, String ipa, String termId) {
        return new CacheEntry(key, termId,
                invokeLexconvert(ipa, IPA_FORMAT, windowsFormat),
                invokeLexconvert(ipa, IPA_FORMAT, macFormat),
                invokeLexconvert(ipa, IPA_FORMAT, linuxFormat));
    }

    public void load() {
        CacheEntry[] entries;
        try {
            entries = gson.fromJson(Files.readLines(file, UTF_8).stream().collect(Collectors.joining()), CacheEntry[].class);
        } catch (IOException e) {
            entries = new CacheEntry[0];
        }

        if(entries == null) entries = new CacheEntry[0];

        this.entries.clear();

        for (CacheEntry e : entries) {
            this.entries.put(e.getKey(), e);
        }
    }

    public void save() {
        try {
            String json = gson.toJson(entries.values().toArray());
            Files.write(json, file, UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String invokeLexconvert(String input, String inputFormat, String outputFormat) {
        if(!canInvoke()) throw new RuntimeException("Tried to run lexconvert.py, but it is not configured! See <URL>" +
                "for more information"); // TODO: add url
        String[] args = { pythonExecutablePath, lexconvertPath, "--phones2phones", inputFormat, outputFormat, input };
        PronunciationGuide.logger.info("Invoking lexconvert.py with arguments " + Arrays.toString(args));
        ProcessBuilder pb = new ProcessBuilder(args);

        BufferedReader stdoutReader = null, stderrReader = null;

        try {
            Process p = pb.start();

            stdoutReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            stderrReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            List<String> stdoutLines = stdoutReader.lines().collect(Collectors.toList());
            List<String> stderrLines = stderrReader.lines().collect(Collectors.toList());

            if (!stderrLines.isEmpty()) {
                PronunciationGuide.logger.error("lexconvert.py errored during invocation!");
                for (String line : stderrLines) {
                    PronunciationGuide.logger.error("[lexconvert.py stderr] " + line);
                }
            }

            if (stdoutLines.size() > 1) {
                PronunciationGuide.logger.warn("lexconvert.py produced more than one output line!");
                for (String line : stdoutLines) {
                    PronunciationGuide.logger.warn("[lexconvert.py stdout] " + line);
                }
                PronunciationGuide.logger.warn("(regarding previous messages) The first of the output lines will be used.");
            }

            if (stdoutLines.size() == 0) {
                PronunciationGuide.logger.error("lexconvert.py produced no output!");
                throw new RuntimeException("lexconvert.py produced no output! Using null instead");
            }

            return stdoutLines.get(0);
        } catch(IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if(stdoutReader != null) try {
                stdoutReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(stderrReader != null) try {
                stderrReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean canInvoke() {
        return !pythonExecutablePath.equals("") && !lexconvertPath.equals("");
    }
}
