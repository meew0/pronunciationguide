package meew0.pguide.lexconvert;

/**
 * Created by meew0 on 04/08/18.
 */
public class CacheEntry {
    private String key, termId;
    private String windows, mac, linux;

    public CacheEntry(String key, String termId, String windows, String mac, String linux) {
        this.key = key;
        this.termId = termId;
        this.windows = windows;
        this.mac = mac;
        this.linux = linux;
    }

    public String getKey() {
        return key;
    }

    public String getTermId() {
        return termId;
    }

    public String getFormat(PlatformUtils.Platform platform) {
        return PlatformUtils.resolvePlatformSpecific(platform, windows, mac, linux);
    }
}
