package meew0.pguide;

import com.google.common.collect.Iterators;
import meew0.pguide.lib.ITrie;
import meew0.pguide.lib.SingleUseIterable;
import meew0.pguide.lib.Term;
import meew0.pguide.lib.Trie;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.GuiScrollingList;
import net.minecraftforge.fml.client.config.GuiUtils;
import org.lwjgl.input.Mouse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by meew0 on 04/08/18.
 */
public class GuiSearch extends GuiScreen {
    private static final ResourceLocation BACKGROUND_TEXTURE =
            new ResourceLocation("pronunciationguide:textures/gui_search.png");

    private static final int PANEL_WIDTH = 140;
    private static final int PANEL_HEIGHT = 103;
    private int panelTop, panelLeft;

    private TermList termList;
    private GuiTextField searchBox;

    private List<Term> currentView;
    private final Trie<Term> index;
    private ITrie<Term> currentSubTrie;

    public GuiSearch() {
        currentView = PronunciationGuide.TERM_REGISTRY.getSortedList();
        index = new Trie<>(currentView, term -> {
            String lower = term.getLocalised().toLowerCase();
            List<Integer> indicesOfSpaces = new ArrayList<>();
            indicesOfSpaces.add(0);
            int lastIndex = lower.indexOf(' ');
            while(lastIndex > -1) {
                indicesOfSpaces.add(lastIndex + 1);
                lastIndex = lower.indexOf(' ', lastIndex + 1);
            }

            return SingleUseIterable.of(indicesOfSpaces.stream().map(lower::substring).iterator());
        });
        currentSubTrie = index;
    }

    @Override
    public void initGui() {
        panelLeft = (width - PANEL_WIDTH) / 2;
        panelTop = (height - PANEL_HEIGHT) / 2;

        this.termList = new TermList(Minecraft.getMinecraft(),
                132, 80, panelTop + 19, panelTop + 100, panelLeft + 5, 21,
                width, height);

        searchBox = new GuiTextField(0, mc.fontRenderer, panelLeft + 3, panelTop + 3, 134, 14);
        searchBox.setFocused(true);
        searchBox.setCanLoseFocus(false);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        drawDefaultBackground();

        GlStateManager.color(1f, 1f, 1f);
        mc.getTextureManager().bindTexture(BACKGROUND_TEXTURE);
        drawTexturedModalRect(panelLeft, panelTop, 0, PANEL_HEIGHT, PANEL_WIDTH, PANEL_HEIGHT);

        super.drawScreen(mouseX, mouseY, partialTicks);
        termList.drawScreen(mouseX, mouseY, partialTicks);

        // Draw the "shadow" of the outer frame on the inner part. For some reason drawTexturedModalRect doesn't work
        // with semi transparent textures, so we just do it like this
        final int COLOUR = 94 << 24; // Black with alpha 94
        GuiUtils.drawGradientRect(3, panelLeft + 3, panelTop + 20, panelLeft + 4, panelTop + 99, COLOUR, COLOUR);
        GuiUtils.drawGradientRect(3, panelLeft + 4, panelTop + 19, panelLeft + 136, panelTop + 20, COLOUR, COLOUR);
        GuiUtils.drawGradientRect(3, panelLeft + 4, panelTop + 20, panelLeft + 5, panelTop + 21, COLOUR, COLOUR);

        mc.getTextureManager().bindTexture(BACKGROUND_TEXTURE);
        drawTexturedModalRect(panelLeft, panelTop, 0, 0, PANEL_WIDTH, PANEL_HEIGHT);

        searchBox.drawTextBox();
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        searchBox.updateCursorCounter();
    }

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();

        int mouseX = Mouse.getEventX() * width / mc.displayWidth;
        int mouseY = height - Mouse.getEventY() * height / mc.displayHeight - 1;
        termList.handleMouseInput(mouseX, mouseY);
    }

    private String lastText = "";

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        searchBox.textboxKeyTyped(typedChar, keyCode);

        String text = searchBox.getText();
        if(text.isEmpty()) {
            currentView = PronunciationGuide.TERM_REGISTRY.getSortedList();
            currentSubTrie = index;
        } else {
            if(text.startsWith(lastText) && lastText.length() == text.length() - 1) {
                currentSubTrie = currentSubTrie.getDirectSubTrie(text.charAt(text.length() - 1));
            } else {
                currentSubTrie = index.getSubTrie(text);
            }
            currentView = currentSubTrie.getValuesSorted();
        }

        lastText = text;
    }

    private class TermList extends GuiScrollingList {

        public TermList(Minecraft client, int width, int height, int top, int bottom, int left, int entryHeight,
                        int screenWidth, int screenHeight) {
            super(client, width, height, top, bottom, left, entryHeight, screenWidth, screenHeight);
        }

        @Override
        protected int getSize() {
            return currentView.size();
        }

        @Override
        protected void elementClicked(int index, boolean doubleClick) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiPronunciationGuide(currentView.get(index)));
        }

        @Override
        protected boolean isSelected(int index) {
            return false;
        }

        @Override
        protected void drawBackground() {

        }

        @Override
        protected void drawGradientRect(int left, int top, int right, int bottom, int color1, int color2) {
            // No-op, otherwise there would be a semi-transparent background
        }

        @Override
        protected void drawSlot(int index, int entryRight, int slotTop, int slotBuffer, Tessellator tess) {
            Term term = currentView.get(index);

            mc.getTextureManager().bindTexture(BACKGROUND_TEXTURE);
            drawTexturedModalRect(left + 1, slotTop + 1, 140, 0, 19, 19);

            int mouseX = Mouse.getX() * width / mc.displayWidth;
            int mouseY = height - Mouse.getY() * height / mc.displayHeight - 1;

            // Highlight slot being hovered over
            int textColour = 0xaaaaaa;
            if(mouseX > left && mouseX <= left + listWidth - 7 /* scroll bar */ && mouseY > slotTop &&
                    mouseY <= slotTop + slotHeight)
                textColour = 0xffffff;

            int x = left + 23;
            final int y = slotTop + 6;
            if(!searchBox.getText().isEmpty()) {
                String openQuote = I18n.format("pguide.general.openquote"),
                        closeQuote = I18n.format("pguide.general.closequote");
                String localised = term.getLocalised();
                int matchIndex = localised.toLowerCase().indexOf(" " + searchBox.getText().toLowerCase()) + 1;
                if(matchIndex == -1) matchIndex = 0;
                int matchLength = searchBox.getText().length();
                x = mc.fontRenderer.drawStringWithShadow(openQuote, x, y, textColour) - 1;
                x = mc.fontRenderer.drawStringWithShadow(localised.substring(0, matchIndex), x, y, textColour) - 1;
                x = mc.fontRenderer.drawStringWithShadow(localised.substring(matchIndex, matchIndex + matchLength), x, y, 0xff5555) - 1;
                x = mc.fontRenderer.drawStringWithShadow(localised.substring(matchIndex + matchLength), x, y, textColour) - 1;
                mc.fontRenderer.drawStringWithShadow(closeQuote, x, y, textColour);
            } else {
                mc.fontRenderer.drawStringWithShadow(I18n.format("pguide.general.quotedstring", term.getLocalised()), x, y, textColour);
            }

            RenderHelper.enableGUIStandardItemLighting();
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0F, 240.0F);
            itemRender.renderItemAndEffectIntoGUI(term.getDisplayItemStack(), left + 2, slotTop + 2);
        }
    }
}
