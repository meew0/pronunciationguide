package meew0.pguide.lib;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by meew0 on 05/08/18.
 */
public interface ITrie<T> {
    @Nullable T get(String key);
    @Nonnull ITrie<T> getSubTrie(String key);
    @Nonnull ITrie<T> getDirectSubTrie(char c);
    @Nullable T getValue();
    @Nonnull List<T> getValuesSorted();
}
