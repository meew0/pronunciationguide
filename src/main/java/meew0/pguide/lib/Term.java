package meew0.pguide.lib;

import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by meew0 on 17/07/18.
 */
public class Term {
    private final String id, modId;
    private ItemStack displayItemStack = ItemStack.EMPTY;

    Term(String id, String modId) {
        this.id = id;
        this.modId = modId;
    }

    public String getId() {
        return id;
    }

    public String getModId() {
        return modId;
    }

    public ItemStack getDisplayItemStack() {
        return displayItemStack;
    }

    public void setDisplayItemStack(ItemStack displayItemStack) {
        this.displayItemStack = displayItemStack;
    }

    private String getUnlocalised() {
        return modId + ".term." + id;
    }

    public String getLocalised() {
        return I18n.format( getUnlocalised() + ".name");
    }

    public List<String> getAlternativesUnlocalised() {
        if(I18n.hasKey(getUnlocalised())) {
            return Collections.singletonList(getUnlocalised());
        }

        ArrayList<String> alternatives = new ArrayList<>();

        for(int i = 1; I18n.hasKey(getUnlocalised() + "." + i); i++) {
            alternatives.add(getUnlocalised() + "." + i);
        }

        return alternatives;
    }

    public List<String> getAlternativesLocalised() {
        return getAlternativesUnlocalised().stream().map(alt -> I18n.format(alt)).collect(Collectors.toList());
    }
}
