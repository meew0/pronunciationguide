package meew0.pguide.lib;

import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.*;

/**
 * Created by meew0 on 17/07/18.
 */
public class TermRegistry {
    private final HashMap<String, Term> termsById = new HashMap<>();
    private final HashMap<ResourceLocation, Term> termsByAssociatedBlock = new HashMap<>();
    private final HashMap<ResourceLocation, Term> termsByAssociatedItemRegistryName = new HashMap<>();
    private final HashMap<String, Term> termsByAssociatedItemUnlocalizedName = new HashMap<>();

    private ArrayList<Term> sortedTerms = new ArrayList<>();
    private boolean needsSorting = false;

    @Nullable
    public Term getTermById(String id) {
        return termsById.get(id);
    }

    public Term getTermByIndex(int index)  {
        return sortedTerms.get(index);
    }

    public boolean termExists(String id) {
        return termsById.containsKey(id);
    }

    public Term registerTerm(Term term) {
        termsById.put(term.getId(), term);

        sortedTerms.add(term);
        if(needsSorting) sort();

        return term;
    }

    public Term registerTerm(String id, String modId) {
        return registerTerm(new Term(id, modId));
    }

    public Collection<Term> getAllTerms() {
        return termsById.values();
    }

    public void associateTermWithBlock(Term term, ResourceLocation blockRegistryName) {
        termsByAssociatedBlock.put(blockRegistryName, term);
    }

    public void associateTermWithItemRegistryName(Term term, ResourceLocation itemRegistryName) {
        termsByAssociatedItemRegistryName.put(itemRegistryName, term);
    }

    public void associateTermWithItemUnlocalizedName(Term term, String itemUnlocalizedName) {
        termsByAssociatedItemUnlocalizedName.put(itemUnlocalizedName, term);
    }

    @Nullable
    public Term getAssociatedTerm(ItemStack stack) {
        if(stack == null || stack.isEmpty()) return null;

        String unlocalizedName = stack.getUnlocalizedName();
        if(termsByAssociatedItemUnlocalizedName.containsKey(unlocalizedName)) {
            return termsByAssociatedItemUnlocalizedName.get(unlocalizedName);
        } else {
            // may be null
            return termsByAssociatedItemRegistryName.get(stack.getItem().getRegistryName());
        }
    }

    @Nullable
    public Term getAssociatedTerm(IBlockState blockState) {
        System.out.println(blockState.getBlock().getRegistryName());
        System.out.println(Arrays.toString(termsByAssociatedBlock.keySet().toArray()));
        return termsByAssociatedBlock.get(blockState.getBlock().getRegistryName());
    }

    public int size() {
        return termsById.size();
    }

    public void enableSorting() {
        needsSorting = true;
        sort();
    }

    private void sort() {
        sortedTerms.sort(Comparator.comparing(Term::getLocalised));
    }

    public List<Term> getSortedList() {
        return sortedTerms;
    }
}
