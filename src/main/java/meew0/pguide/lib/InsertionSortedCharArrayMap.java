package meew0.pguide.lib;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import scala.Char;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Sorted map of char -> V represented by an array that is sorted on insert time, useful for cases where the map is
 * small and rarely modified.
 */
public class InsertionSortedCharArrayMap<V> implements Iterable<V> {
    private final ObjectArrayList<Entry> values;

    public InsertionSortedCharArrayMap() {
        values = new ObjectArrayList<>(1);
    }

    public void put(char key, V value) {
        values.add(new Entry(key, value));
        values.sort(Comparator.comparingInt(Entry::getKey));
    }

    @Nullable
    public V get(char key) {
        int size = values.size();

        if(size == 1) {
            Entry e = values.get(0);
            return e.key == key ? e.value : null;
        }

        int lowerIdx = 0, upperIdx = size;
        while(lowerIdx != upperIdx) {
            int middle = lowerIdx + (upperIdx - lowerIdx) / 2;

            Entry e = values.get(middle);
            char newKey = e.key;
            if(newKey == key) return e.value;

            if(newKey < key) {
                if(lowerIdx == middle) return null;
                lowerIdx = middle;
            }
            if(newKey > key) {
                if(upperIdx == middle) return null;
                upperIdx = middle;
            }
        }

        return null;
    }

    @Override
    @Nonnull
    public Iterator<V> iterator() {
        return values.stream().map(Entry::getValue).iterator();
    }

    @Override
    public void forEach(Consumer<? super V> consumer) {
        values.stream().map(Entry::getValue).forEach(consumer);
    }

    @Override
    public Spliterator<V> spliterator() {
        return values.stream().map(Entry::getValue).spliterator();
    }

    private class Entry {
        private final char key;
        private final V value;

        private Entry(char key, V value) {
            this.key = key;
            this.value = value;
        }

        public char getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }
}
