package meew0.pguide.lib;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by meew0 on 05/08/18.
 */
public class SingleUseIterable<T> implements Iterable<T> {
    private final Iterator<T> iterator;
    private boolean used = false;

    private SingleUseIterable(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    public static <T> SingleUseIterable<T> of(Iterator<T> iterator) {
        return new SingleUseIterable<>(iterator);
    }

    @Nonnull
    @Override
    public Iterator<T> iterator() {
        if(used) throw new IllegalStateException("SingleUseIterable has already been used");
        used = true;
        return iterator;
    }
}
