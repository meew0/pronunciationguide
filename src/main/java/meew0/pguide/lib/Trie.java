package meew0.pguide.lib;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * Implementation of an immutable trie, for term search purposes
 */
public class Trie<T> implements ITrie<T> {
    private InsertionSortedCharArrayMap<Trie<T>> children;
    private T value = null;

    private Trie() {
        children = new InsertionSortedCharArrayMap<>();
    }

    public Trie(List<T> list, Function<T, Iterable<String>> keyProvider) {
        this();
        for(T element : list) {
            for(String key : keyProvider.apply(element)) {
                add(key, element);
            }
        }
    }

    private void add(String key, T value) {
        add(key, 0, value);
    }

    private void add(String key, int index, T value) {
        if(index == key.length()) {
            this.value = value;
            return;
        }

        char c = key.charAt(index);
        Trie<T> child = children.get(c);
        if(child != null) {
            child.add(key, index + 1, value);
        } else {
            Trie<T> newChild = new Trie<>();
            newChild.add(key, index + 1, value);
            children.put(c, newChild);
        }
    }

    @Nullable
    public T get(String key) {
        return getSubTrie(key).getValue();
    }

    @Nonnull
    public ITrie<T> getSubTrie(String key) {
        return getSubTrie(key, 0);
    }

    @Nonnull
    private ITrie<T> getSubTrie(String key, int index) {
        if(index == key.length()) {
            return this;
        }

        char c = key.charAt(index);
        Trie<T> child = children.get(c);
        if(child != null) {
            return child.getSubTrie(key, index + 1);
        } else return new EmptyTrie<>();
    }

    @Nonnull
    public ITrie<T> getDirectSubTrie(char c) {
        Trie<T> child = children.get(c);
        if(child != null) {
            return child;
        } else return new EmptyTrie<>();
    }

    @Nullable
    public T getValue() {
        return value;
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public List<T> getValuesSorted() {
        ObjectArrayList<Object> untyped = getValuesSortedInternal();
        List<T> result = new ObjectArrayList<>(untyped.size());
        for(Object o : untyped) {
            result.add((T) o);
        }

        return result;
    }

    private ObjectArrayList<Object> getValuesSortedInternal() {
        ObjectArrayList<Object> result = new ObjectArrayList<>();

        if(value != null) result.add(value);
        for(Trie<T> child : children) {
            ObjectArrayList<Object> subList = child.getValuesSortedInternal();
            Object[] subListArray = new Object[subList.size()];
            subList.getElements(0, subListArray, 0, subList.size());
            result.addElements(result.size(), subListArray, 0, subListArray.length);
        }

        return result;
    }

    public static class EmptyTrie<T> implements ITrie<T> {
        private EmptyTrie() {}

        @Nullable
        @Override
        public T get(String key) {
            return null;
        }

        @Nonnull
        @Override
        public ITrie<T> getSubTrie(String key) {
            return this;
        }

        @Nonnull
        @Override
        public ITrie<T> getDirectSubTrie(char c) {
            return this;
        }

        @Nullable
        @Override
        public T getValue() {
            return null;
        }

        @Nonnull
        @Override
        public List<T> getValuesSorted() {
            return Collections.emptyList();
        }
    }
}
