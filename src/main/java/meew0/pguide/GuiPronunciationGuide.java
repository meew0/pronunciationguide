package meew0.pguide;

import meew0.pguide.ipa.IPhoneme;
import meew0.pguide.ipa.PhonemeParser;
import meew0.pguide.lexconvert.PlatformUtils;
import meew0.pguide.lib.Term;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiUtils;
import org.lwjgl.input.Mouse;

import java.io.IOException;
import java.util.*;

/**
 * Created by meew0 on 17/07/18.
 */
public class GuiPronunciationGuide extends GuiScreen {
    private static final ResourceLocation BACKGROUND_TEXTURE =
            new ResourceLocation("pronunciationguide:textures/gui_background.png");

    private int panelWidth = 140;
    private int panelHeight = 85;

    private static final int COLOR_GRAY = 0x777777;
    private static final int COLOR_WHITE = 0xffffff;
    private static final int COLOR_PHONEME_STATIC = COLOR_GRAY;
    private static final int COLOR_PHONEME_DEFAULT = 0xaa1111;
    private static final int COLOR_PHONEME_HIGHLIGHT = 0xff5555;
    private static final int COLOR_OR = 0x555555;

    private static final int IPA_HEIGHT = 16;

    private final Term term;
    private final PhonemeParser parser;
    private final List<IPhoneme[]> parsedPhonemes = new ArrayList<>();
    private final TreeMap<Integer, TreeMap<Integer, PhonemeRenderData>> phonemeLocations = new TreeMap<>();
    private final WeakHashMap<IPhoneme, String> phonemeReverseAssoc = new WeakHashMap<>();

    private IPhoneme highlightedPhoneme = null;
    private boolean phonemeLocationsCalculated = false;

    private int phonemeMaxX, phonemeMaxY;
    private int panelTop, panelLeft;

    // for detecting when the window scale was changed, so we can recalculate phoneme locations
    private int lastWidth, lastHeight;

    private int offsetDueToOr = 0;

    public static ScaledFontRenderer ipaFontRenderer;

    public GuiPronunciationGuide(Term term) {
        super();
        this.term = term;
        this.parser = new PhonemeParser();

        for(String alt : term.getAlternativesUnlocalised()) {
            String ipa = I18n.format(alt);
            IPhoneme[] parsed = parser.parse(ipa);
            parsedPhonemes.add(parsed);

            for(IPhoneme phoneme : parsed) {
                phonemeReverseAssoc.put(phoneme, alt);
            }
        }
    }

    @Override
    public void initGui() {
        determinePanelSizing();
        offsetDueToOr = mc.fontRenderer.getStringWidth(I18n.format("pguide.general.or"));
    }

    private void determinePanelSizing() {
        panelLeft = panelTop = 0;
        phonemeMaxX = phonemeMaxY = 0;

        ipaFontRenderer.setInvis(true);
        calculatePhonemeLocationsAndDraw();
        ipaFontRenderer.setInvis(false);

        panelWidth = phonemeMaxX + 7;
        panelHeight = phonemeMaxY + 7;

        panelLeft = (width - panelWidth) / 2;
        panelTop = (height - panelHeight) / 2;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        if(lastHeight != height || lastWidth != width) {
            lastWidth = width;
            lastHeight = height;
            determinePanelSizing();
            phonemeLocationsCalculated = false;
        }

        drawDefaultBackground();
        drawDynamicBackground();

        super.drawScreen(mouseX, mouseY, partialTicks);

        String quoted = I18n.format("pguide.general.quotedstring", term.getLocalised());
        mc.fontRenderer.drawStringWithShadow(quoted, panelLeft + 28, panelTop + 11, COLOR_WHITE);

        if(!phonemeLocationsCalculated) {
            calculatePhonemeLocationsAndDraw();
        } else {
            drawPhonemesFromLocations();
        }

        drawOrs();

        if(highlightedPhoneme != null) drawPhonemeTooltip(highlightedPhoneme);

        RenderHelper.enableGUIStandardItemLighting();
        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0F, 240.0F);
        itemRender.renderItemAndEffectIntoGUI(term.getDisplayItemStack(), panelLeft + 7, panelTop + 7);
    }

    private void drawDynamicBackground() {
        GlStateManager.color(1f, 1f, 1f);
        mc.getTextureManager().bindTexture(BACKGROUND_TEXTURE);

        // Slot icon + upper left corner
        drawOffsetModalRect(0, 0, 0, 0, 25, 25);

        // Upper border
        tile(25, 0, 25, 0, 227, 25, panelWidth - 25 - 4, 25);

        // Upper right corner
        drawOffsetModalRect(panelWidth - 4, 0, 252, 0, 4, 4);

        // Right border
        tile(panelWidth - 4, 4, 252, 4, 4, 248, 4, panelHeight - 8);

        // Lower right corner
        drawOffsetModalRect(panelWidth - 4, panelHeight - 4, 252, 252, 4, 4);

        // Lower border
        tile(4, panelHeight - 4, 4, 252, 248, 4, panelWidth - 8, 4);

        // Lower left corner
        drawOffsetModalRect(0, panelHeight - 4, 0, 252, 4, 4);

        // Left border
        tile(0, 25, 0, 25, 25, 227, 25, panelHeight - 25 - 4);

        // Inside
        tile(25, 25, 25, 25, 227, 227, panelWidth - 25 - 4, panelHeight - 25 - 4);
    }

    private void tile(int offsetX, int offsetY, int textureX, int textureY, int textureWidth, int textureHeight, int actualWidth, int actualHeight) {
        int y = offsetY, yCount = actualHeight / textureHeight, yRem = actualHeight % textureHeight;
        for(int i = 1; i <= yCount; i++) {
            tileX(offsetX, y, textureX, textureY, textureWidth, textureHeight, actualWidth);
            y = offsetY + i * textureHeight;
        }
        if(yRem > 0) tileX(offsetX, y, textureX, textureY, textureWidth, yRem, actualWidth);
    }

    private void tileX(int offsetX, int offsetY, int textureX, int textureY, int textureWidth, int textureHeight, int actualWidth) {
        int x = offsetX, xCount = actualWidth / textureWidth, xRem = actualWidth % textureWidth;
        for(int i = 1; i <= xCount; i++) {
            drawOffsetModalRect(x, offsetY, textureX, textureY, textureWidth, textureHeight);
            x = offsetX + i * textureWidth;
        }
        if(xRem > 0) drawOffsetModalRect(x, offsetY, textureX, textureY, xRem, textureHeight);
    }

    private void drawOffsetModalRect(int offsetX, int offsetY, int textureX, int textureY, int width, int height) {
        drawTexturedModalRect(panelLeft + offsetX, panelTop + offsetY, textureX, textureY, width, height);
    }

    private void drawPhonemeTooltip(IPhoneme phoneme) {
        if(phoneme.getDescription() == null) return;

        String formatted = phoneme.getDescription()
                .replace("[", "§c")
                .replace("]", "§r");

        GuiUtils.drawHoveringText(Collections.singletonList(formatted), lastMouseX, lastMouseY, width, height, 120, mc.fontRenderer);
    }

    private void drawOrs() {
        boolean isFirst = true;

        for(int y : phonemeLocations.navigableKeySet()) {
            if(!isFirst) {
                mc.fontRenderer.drawStringWithShadow(I18n.format("pguide.general.or"), panelLeft + 7, y + 6, COLOR_OR);
            }

            isFirst = false;
        }
    }

    private void drawPhonemesFromLocations() {
        phonemeLocations.forEach(
                (y, row) -> row.forEach(
                        (x, rd) ->
                                drawPhonemeAtLocation(rd.getPhoneme(), x, y)));
    }

    private static final int LINE_OFFSET = 4;

    private void calculatePhonemeLocationsAndDraw() {
        phonemeLocations.clear();

        int y = panelTop + 26;
        int xRoot = panelLeft + 7 + offsetDueToOr + 6;
        int xOffset = 0;

        for(IPhoneme[] phonemes : parsedPhonemes) {
            TreeMap<Integer, PhonemeRenderData> currentLineLocations = new TreeMap<>();

            for(IPhoneme phoneme : phonemes) {
                int x = xRoot + xOffset;
                int newX = drawPhonemeAtLocation(phoneme, x, y);
                if(newX > phonemeMaxX) phonemeMaxX = newX;
                int width = newX - x;

                PhonemeRenderData rd = new PhonemeRenderData(phoneme, width);
                currentLineLocations.put(x, rd);

                xOffset += width;
            }

            phonemeLocations.put(y, currentLineLocations);
            y += (IPA_HEIGHT + LINE_OFFSET);
            xOffset = 0;
        }

        if(y > phonemeMaxY) phonemeMaxY = y;
        phonemeLocationsCalculated = true;
    }

    private int drawPhonemeAtLocation(IPhoneme phoneme, int x, int y) {
        int color;

        if(phoneme.isHighlightable()) {
            color = (phoneme == highlightedPhoneme) ? COLOR_PHONEME_HIGHLIGHT : COLOR_PHONEME_DEFAULT;
        } else {
            color = COLOR_PHONEME_STATIC;
        }

        return ipaFontRenderer.drawStringWithShadow(phoneme.getIPA(), x, y, color);
    }

    public void listen() {
        String alt = phonemeReverseAssoc.get(highlightedPhoneme);
        if(alt == null) return;
        String narratorKey = PronunciationGuide.lexconvertCache.get(alt, I18n.format(alt), term.getId());
        if(narratorKey != null) {
            PronunciationGuide.narrator.clear();
            PronunciationGuide.narrator.say(narratorKey);
        } else {
            PronunciationGuide.logger.warn("Narration unavailable for " + alt + ", platform: " + PlatformUtils.getCurrentPlatform());
        }
    }

    private int lastMouseX = 0, lastMouseY = 0;

    @Override
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();

        int mouseX = Mouse.getEventX() * width / mc.displayWidth;
        int mouseY = height - Mouse.getEventY() * height / mc.displayHeight - 1;

        if(mouseX == lastMouseX && mouseY == lastMouseY) return;

        lastMouseX = mouseX;
        lastMouseY = mouseY;

        IPhoneme phonemeAtMouse = getPhonemeAt(mouseX, mouseY - 3);
        if(phonemeAtMouse == null) {
            highlightedPhoneme = null;
        } else {
            highlightedPhoneme = phonemeAtMouse;
        }
    }

    private IPhoneme getPhonemeAt(int mouseX, int mouseY) {
        NavigableSet<Integer> ySet = phonemeLocations.navigableKeySet();

        Integer y = ySet.floor(mouseY);
        if(y == null) return null;

        TreeMap<Integer, PhonemeRenderData> xMap = phonemeLocations.get(y);
        NavigableSet<Integer> xSet = xMap.navigableKeySet();

        Integer x = xSet.floor(mouseX);
        if(x == null) return null;

        PhonemeRenderData rd = xMap.get(x);
        if(mouseX < x + rd.getWidth() && mouseY < y + IPA_HEIGHT) {
            return rd.getPhoneme();
        }

        return null;
    }

    public static void initialiseIPAFontRenderer() {
        Minecraft mc = Minecraft.getMinecraft();

        ipaFontRenderer = new ScaledFontRenderer(
                mc.gameSettings,

                // this doesn't matter anyway as we'll only ever use the unicode font
                new ResourceLocation("textures/font/ascii_sga.png"),
                mc.renderEngine,
                true,
                2f
        );
    }

    private class PhonemeRenderData {
        private final IPhoneme phoneme;
        private final int width;

        private PhonemeRenderData(IPhoneme phoneme, int width) {
            this.phoneme = phoneme;
            this.width = width;
        }

        public IPhoneme getPhoneme() {
            return phoneme;
        }

        public int getWidth() {
            return width;
        }
    }
}
