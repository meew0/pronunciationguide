package meew0.pguide;

import meew0.pguide.lexconvert.CacheRebuildRunner;
import meew0.pguide.lexconvert.CacheTestRunner;
import meew0.pguide.lexconvert.TaskRunner;
import meew0.pguide.lib.Term;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderTooltipEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.client.settings.KeyModifier;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by meew0 on 17/07/18.
 */
public class ClientHandler {
    private final KeyBinding openGuide, openSearch, listen;
    private KeyBinding advToolsMod, adv_rebuildLCC, adv_testNarrator, adv_testAllCacheEntries, adv_forceReloadLCC, adv_forceSaveLCC;
    private final TaskRunner cacheRebuild = new CacheRebuildRunner(), cacheTest = new CacheTestRunner();
    private int unavailableTooltipTime = 0;
    private boolean currentlyRenderingUnavailableTooltip = false;
    private ItemStack lastItemStack = ItemStack.EMPTY;

    public ClientHandler() {
        openGuide = new KeyBinding("pronunciationguide.keybind.openGuide", KeyConflictContext.UNIVERSAL, Keyboard.KEY_P, "key.categories.pronunciationguide");
        ClientRegistry.registerKeyBinding(openGuide);

        openSearch = new KeyBinding("pronunciationguide.keybind.openSearch", KeyConflictContext.UNIVERSAL, Keyboard.KEY_U, "key.categories.pronunciationguide");
        ClientRegistry.registerKeyBinding(openSearch);

        listen = new KeyBinding("pronunciationguide.keybind.listen", KeyConflictContext.GUI, Keyboard.KEY_O, "key.categories.pronunciationguide");
        ClientRegistry.registerKeyBinding(listen);

        if(PronunciationGuide.enableDebug) {
            advToolsMod = new KeyBinding("pronunciationguide.keybind.advToolsMod", KeyConflictContext.IN_GAME, KeyModifier.CONTROL, Keyboard.KEY_X, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(advToolsMod);

            adv_rebuildLCC = new KeyBinding("pronunciationguide.keybind.adv_rebuildLCC", KeyConflictContext.IN_GAME, Keyboard.KEY_NONE, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(adv_rebuildLCC);

            adv_testNarrator = new KeyBinding("pronunciationguide.keybind.adv_testNarrator", KeyConflictContext.IN_GAME, Keyboard.KEY_NONE, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(adv_testNarrator);

            adv_testAllCacheEntries = new KeyBinding("pronunciationguide.keybind.adv_testAllCacheEntries", KeyConflictContext.IN_GAME, Keyboard.KEY_NONE, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(adv_testAllCacheEntries);

            adv_forceReloadLCC = new KeyBinding("pronunciationguide.keybind.adv_forceReloadLCC", KeyConflictContext.IN_GAME, Keyboard.KEY_NONE, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(adv_forceReloadLCC);

            adv_forceSaveLCC = new KeyBinding("pronunciationguide.keybind.adv_forceSaveLCC", KeyConflictContext.IN_GAME, Keyboard.KEY_NONE, "key.categories.pronunciationguide_advtools");
            ClientRegistry.registerKeyBinding(adv_forceSaveLCC);
        }
    }

    @SubscribeEvent
    public void onKeyboardInputPost(GuiScreenEvent.KeyboardInputEvent.Post event) {
        Minecraft mc = Minecraft.getMinecraft();
        GuiScreen currentGui = mc.currentScreen;

        if(currentGui == null) return;

        if(currentGui instanceof GuiPronunciationGuide) {
            if(listen.isActiveAndMatches(Keyboard.getEventKey())) {
                ((GuiPronunciationGuide) currentGui).listen();
            }
            return;
        }

        if(openGuide.isActiveAndMatches(Keyboard.getEventKey())) {
            Term term = null;

            if (currentGui instanceof GuiContainer) {
                ItemStack stack = getItemStackUnderMouse((GuiContainer) currentGui);
                term = PronunciationGuide.TERM_REGISTRY.getAssociatedTerm(stack);
            }

            if(term == null) {
                // Show tooltip
                unavailableTooltipTime = 100;
            } else {
                mc.displayGuiScreen(new GuiPronunciationGuide(term));
            }

            event.setCanceled(true);
        }
    }

    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event) {
        Minecraft mc = Minecraft.getMinecraft();
        GuiScreen currentGui = mc.currentScreen;

        if(unavailableTooltipTime > 0) {
            unavailableTooltipTime--;

            if(currentGui instanceof GuiPronunciationGuide) unavailableTooltipTime = 0;
            if(unavailableTooltipTime == 0) lastItemStack = ItemStack.EMPTY;
        }

        if(openGuide.isKeyDown()) {
            Term term = null;

            if(currentGui == null) {
                IBlockState blockState = getStateOfBlockLookedAt();
                if(blockState != null) term = PronunciationGuide.TERM_REGISTRY.getAssociatedTerm(blockState);
            }

            if(term == null) {
                ITextComponent msg = new TextComponentTranslation("pguide.general.unavailable.ingame");
                msg.setStyle(new Style().setColor(TextFormatting.RED));
                mc.player.sendStatusMessage(msg, true);
            } else {
                mc.displayGuiScreen(new GuiPronunciationGuide(term));
            }
        }

        if(openSearch.isPressed()) {
            mc.displayGuiScreen(new GuiSearch());
        }

        if(advToolsMod.isKeyDown() && mc.gameSettings.showDebugInfo) {
            if(adv_rebuildLCC.isPressed()) {
                new Thread(cacheRebuild, "pguide-rebuild").start();
            } else if(adv_testNarrator.isPressed()) {
                PronunciationGuide.narrator.say("The quick brown fox jumps over the lazy dog");
            } else if(adv_testAllCacheEntries.isPressed()) {
                new Thread(cacheTest, "pguide-test").start();
            } else if(adv_forceReloadLCC.isPressed()) {
                PronunciationGuide.lexconvertCache.load();
                mc.player.sendMessage(new TextComponentTranslation("pguide.adv.reload"));
            } else if(adv_forceSaveLCC.isPressed()) {
                PronunciationGuide.lexconvertCache.save();
                mc.player.sendMessage(new TextComponentTranslation("pguide.adv.save"));
            }
        }
    }

    @SubscribeEvent
    public void onDrawScreenPost(GuiScreenEvent.DrawScreenEvent.Post event) {
        // Render the "No pronunciation available" tooltip if necessary
        if(unavailableTooltipTime > 0) {
            Minecraft mc = Minecraft.getMinecraft();
            String localised = I18n.format("pguide.general.unavailable.gui");
            FontRenderer fr = mc.fontRenderer;

            int calculatedTextWidth = fr.getStringWidth(localised);
            final int UNAVAILABLE_TOOLTIP_MAX_WIDTH = 100;
            int textWidth = Math.min(calculatedTextWidth, UNAVAILABLE_TOOLTIP_MAX_WIDTH);

            int fakeMouseX = (event.getMouseX() - textWidth / 2) - 4; // Center the box below the mouse
            if(fakeMouseX < -8) fakeMouseX = -8; // So the box goes onto the very left edge of the screen
            int fakeMouseY = event.getMouseY() + 26; // Display below cursor

            // If this were the actual width, Forge would awkwardly offset the box to the left on the right half of the
            // screen. So we pretend the screen is really wide and handle this logic ourselves
            int fakeScreenWidth = Integer.MAX_VALUE;
            if(fakeMouseX + textWidth > event.getGui().width) fakeMouseX = event.getGui().width - textWidth;

            currentlyRenderingUnavailableTooltip = true; // So the colour is changed
            GuiUtils.drawHoveringText(Collections.singletonList(localised), fakeMouseX, fakeMouseY,
                    Integer.MAX_VALUE, event.getGui().height, UNAVAILABLE_TOOLTIP_MAX_WIDTH, fr);
        }
    }

    @SubscribeEvent
    public void onTooltipPre(RenderTooltipEvent.Pre event) {
        if(currentlyRenderingUnavailableTooltip || event.getStack().isEmpty()) return;
        if(unavailableTooltipTime <= 0) return;

        // If we are currently rendering the tooltip for some item stack, check whether it is the same as previously.
        // If the item stack has changed while the unavailable tooltip should be displayed, we need to make that not
        // display anymore.

        // If there is no last item stack, we have only just started displaying the unavailable tooltip, so we need to
        // set the item stack to the one we are currently hovering over
        if(lastItemStack.isEmpty()) lastItemStack = event.getStack();

        // Otherwise, if the stacks are different, stop displaying the unavailable tooltip
        if(event.getStack() != lastItemStack) {
            unavailableTooltipTime = 0;
            lastItemStack = ItemStack.EMPTY;
        }
    }

    @SubscribeEvent
    public void onTooltipColor(RenderTooltipEvent.Color event) {
        if(!currentlyRenderingUnavailableTooltip) return;

        event.setBackground(0xf0100000);
        event.setBorderStart(0x50ff0000);
        event.setBorderEnd(0x50cc0000);

        currentlyRenderingUnavailableTooltip = false;
    }

    private ItemStack getItemStackUnderMouse(GuiContainer currentGui) {
        Slot slotUnderMouse = currentGui.getSlotUnderMouse();
        if(slotUnderMouse == null) return ItemStack.EMPTY;

        ItemStack stack = slotUnderMouse.getStack();
        if(stack.isEmpty()) return ItemStack.EMPTY;
        return stack;
    }

    private IBlockState getStateOfBlockLookedAt() {
        EntityPlayerSP player = Minecraft.getMinecraft().player;
        double reachDistance = player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue();
        float partialTicks = Minecraft.getMinecraft().getRenderPartialTicks();
        RayTraceResult result = player.rayTrace(reachDistance, partialTicks);

        if(result == null) return null;

        if(result.typeOfHit == RayTraceResult.Type.BLOCK) {
            return Minecraft.getMinecraft().world.getBlockState(result.getBlockPos());
        }

        return null;
    }
}
