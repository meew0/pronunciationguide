package meew0.pguide;

import meew0.pguide.lib.Term;
import meew0.pguide.lib.TermRegistry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

/**
 * Created by meew0 on 17/07/18.
 */
public class IMCHandler {
    private static final String TASK_REGISTER_TERM = "registerTerm",
            TASK_ASSOCIATE_BLOCK = "associateBlockWithTerm",
            TASK_ASSOCIATE_ITEM_REGISTRY = "associateRegistryItemWithTerm",
            TASK_ASSOCIATE_ITEM_UNLOCALIZED = "associateUnlocalizedItemWithTerm",
            TASK_SET_DISPLAY_STACK = "setDisplayItemStackForTerm";

    private static final char TASK_ARGUMENT_SEPARATOR = ':';

    public static void handleIMCMessage(FMLInterModComms.IMCMessage message) {
        String key = message.key;

        if(key.startsWith(TASK_REGISTER_TERM)) {
            handleRegisterTermTask(message);
        } else {
            String argument = getIMCKeyArgument(message);
            if(key.startsWith(TASK_ASSOCIATE_BLOCK)) {
                handleAssociateBlockTask(argument, message);
            } else if(key.startsWith(TASK_ASSOCIATE_ITEM_REGISTRY)) {
                handleAssociateItemRegistryTask(argument, message);
            } else if(key.startsWith(TASK_ASSOCIATE_ITEM_UNLOCALIZED)) {
                handleAssociateItemUnlocalizedTask(argument, message);
            } else if(key.startsWith(TASK_SET_DISPLAY_STACK)) {
                handleSetDisplayStackTask(argument, message);
            } else {
                throwInvalidMessageError(message, "unknown task");
            }
        }
    }

    private static String getIMCKeyArgument(FMLInterModComms.IMCMessage message) {
        String key = message.key;
        int separatorIndex = key.indexOf(TASK_ARGUMENT_SEPARATOR);

        if(separatorIndex < 0 || separatorIndex != key.lastIndexOf(TASK_ARGUMENT_SEPARATOR)) {
            throwInvalidMessageError(message, "invalid key: key must be \"" + TASK_REGISTER_TERM +
                    "\" or be a task:argument combination");
        }

        return key.substring(separatorIndex + 1);
    }

    private static void handleRegisterTermTask(FMLInterModComms.IMCMessage message) {
        if(!message.isStringMessage()) throwInvalidMessageError(message, TASK_REGISTER_TERM + " value must be string");

        String value = message.getStringValue();
        if(value.contains(String.valueOf(TASK_ARGUMENT_SEPARATOR))) throwInvalidMessageError(message,
                TASK_REGISTER_TERM + " value cannot contain colons");

        PronunciationGuide.TERM_REGISTRY.registerTerm(value, message.getSender());
    }

    private static void handleAssociateBlockTask(String keyArgument, FMLInterModComms.IMCMessage message) {
        if(!message.isResourceLocationMessage()) throwInvalidMessageError(message, TASK_ASSOCIATE_BLOCK +
                " value must be a ResourceLocation (block.getRegistryName())");

        if(!PronunciationGuide.TERM_REGISTRY.termExists(keyArgument)) throwInvalidMessageError(message,
                "specified term \"" + keyArgument + "\" doesn't exist");

        ResourceLocation value = message.getResourceLocationValue();
        if(!ForgeRegistries.BLOCKS.containsKey(value)) throwInvalidMessageError(message,
                 "specified block \"" + value + "\" doesn't exist");

        PronunciationGuide.TERM_REGISTRY.associateTermWithBlock(
                PronunciationGuide.TERM_REGISTRY.getTermById(keyArgument), value);
    }

    private static void handleAssociateItemRegistryTask(String keyArgument, FMLInterModComms.IMCMessage message) {
        if(!message.isResourceLocationMessage()) throwInvalidMessageError(message, TASK_ASSOCIATE_ITEM_REGISTRY +
                " value must be a ResourceLocation (item.getRegistryName())");

        if(!PronunciationGuide.TERM_REGISTRY.termExists(keyArgument)) throwInvalidMessageError(message,
                "specified term \"" + keyArgument + "\" doesn't exist");

        ResourceLocation value = message.getResourceLocationValue();
        if(!ForgeRegistries.ITEMS.containsKey(value)) throwInvalidMessageError(message,
                "specified item \"" + value + "\" doesn't exist");

        PronunciationGuide.TERM_REGISTRY.associateTermWithItemRegistryName(
                PronunciationGuide.TERM_REGISTRY.getTermById(keyArgument), value);
    }

    private static void handleAssociateItemUnlocalizedTask(String keyArgument, FMLInterModComms.IMCMessage message) {
        if(!(message.isStringMessage() || message.isItemStackMessage())) throwInvalidMessageError(message,
                TASK_ASSOCIATE_ITEM_UNLOCALIZED + " value must be String or ItemStack");

        if(!PronunciationGuide.TERM_REGISTRY.termExists(keyArgument)) throwInvalidMessageError(message,
                "specified term \"" + keyArgument + "\" doesn't exist");

        String value;

        if(message.isItemStackMessage()) {
            value = message.getItemStackValue().getUnlocalizedName();
        } else {
            value = message.getStringValue();
        }

        PronunciationGuide.TERM_REGISTRY.associateTermWithItemUnlocalizedName(
                PronunciationGuide.TERM_REGISTRY.getTermById(keyArgument), value);
    }

    private static void handleSetDisplayStackTask(String keyArgument, FMLInterModComms.IMCMessage message) {
        if(!message.isItemStackMessage()) throwInvalidMessageError(message, TASK_SET_DISPLAY_STACK +
                " value must be an ItemStack");

        if(!PronunciationGuide.TERM_REGISTRY.termExists(keyArgument)) throwInvalidMessageError(message,
                "specified term \"" + keyArgument + "\" doesn't exist");

        Term term = PronunciationGuide.TERM_REGISTRY.getTermById(keyArgument);
        term.setDisplayItemStack(message.getItemStackValue());
    }

    private static void throwInvalidMessageError(FMLInterModComms.IMCMessage message, String reason) {
        String errorMessage = "Mod \"" + message.getSender() + "\" has sent an invalid IMC message! Key: \"" +
                message.key + "\", reason: " + reason + ". Please report this to the offending mod \"" +
                message.getSender() + "\", not to Pronunciation Guide";

        throw new IllegalArgumentException(errorMessage);
    }
}
