package meew0.pguide.ipa;

import net.minecraft.client.resources.I18n;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by meew0 on 17/07/18.
 */
public class PhonemeParser {
    private final boolean shouldReplaceIPAChars;
    private final List<String> allPhonemeIPAs;
    private final int maxPhonemeLength;

    public PhonemeParser() {
        shouldReplaceIPAChars = Boolean.parseBoolean(I18n.format("pguide.enableipacharreplacements"));

        String[] localPhonemesList = I18n.format("pguide.phonemes").split(";");

        // Sort phonemes from longest to shortest, so longer ones (which are usually composed of shorter ones) get
        // matched first
        allPhonemeIPAs = Arrays.stream(localPhonemesList)
                .map(this::getCanonicalConditionally) // canonicalise
                .sorted(Comparator.comparingInt(String::length).reversed())
                .collect(Collectors.toList());

        maxPhonemeLength = allPhonemeIPAs.get(0).length();
    }

    public IPhoneme[] parse(String ipa) {
        ArrayList<IPhoneme> result = new ArrayList<>();

        result.add(new StaticPhoneme("/"));

        for(int i = 0, lastPhonemeLength; i < ipa.length(); i += lastPhonemeLength) {
            for(lastPhonemeLength = Math.min(maxPhonemeLength, ipa.length() - i); lastPhonemeLength >= 1; lastPhonemeLength--) {
                String slice = ipa.substring(i, i + lastPhonemeLength);
                if(allPhonemeIPAs.contains(slice) || lastPhonemeLength == 1) {
                    result.add(createPhoneme(slice));
                    break;
                }
            }
        }

        result.add(new StaticPhoneme("/"));

        return result.stream().toArray(IPhoneme[]::new);
    }

    private IPhoneme createPhoneme(String ipa) {
        String exampleKey = "pguide.phoneme." + ipa + ".example";
        if(I18n.hasKey(exampleKey)) return new PhonemeHasDescription(ipa, I18n.format(exampleKey));

        return new PhonemeNoDescription(ipa);
    }

    private String getCanonicalConditionally(String ipa) {
        if(!shouldReplaceIPAChars) return ipa;

        return IPAUtils.getCanonical(ipa);
    }

    private class PhonemeHasDescription implements IPhoneme {
        private final String ipa, description;

        private PhonemeHasDescription(String ipa, String description) {
            this.ipa = ipa;
            this.description = description;
        }

        @Override
        public String getIPA() {
            return ipa;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public boolean isHighlightable() {
            return true;
        }

        @Override
        public int hashCode() {
            return getIPA().hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || !(o instanceof IPhoneme)) return false;
            IPhoneme that = (IPhoneme) o;
            return Objects.equals(getIPA(), that.getIPA());
        }
    }

    private class PhonemeNoDescription implements IPhoneme {
        private final String ipa;

        private PhonemeNoDescription(String ipa) {
            this.ipa = ipa;
        }

        @Override
        public String getIPA() {
            return ipa;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public boolean isHighlightable() {
            return true;
        }

        @Override
        public int hashCode() {
            return getIPA().hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || !(o instanceof IPhoneme)) return false;
            IPhoneme that = (IPhoneme) o;
            return Objects.equals(getIPA(), that.getIPA());
        }
    }

    private class StaticPhoneme extends PhonemeNoDescription {
        private StaticPhoneme(String ipa) {
            super(ipa);
        }

        @Override
        public boolean isHighlightable() {
            return false;
        }
    }
}
