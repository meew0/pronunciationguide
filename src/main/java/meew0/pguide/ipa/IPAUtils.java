package meew0.pguide.ipa;

import net.minecraft.util.text.TextFormatting;

import java.util.HashMap;

/**
 * Created by meew0 on 17/07/18.
 */
public class IPAUtils {
    private static final HashMap<Character, Character> ipaCharAlternatives = new HashMap<>();

    static {
        // The voiced velar stop should be represented as U+0261 LATIN SMALL LETTER SCRIPT G, to guarantee consistent
        // display across fonts.
        ipaCharAlternatives.put('g', 'ɡ');

        // People may accidentally use a colon instead of the long vowel sign.
        ipaCharAlternatives.put(':', 'ː');

        // People may accidentally use an apostrophe instead of the primary stress symbol.
        ipaCharAlternatives.put('\'', 'ˈ');

        // People may accidentally use a comma instead of the secondary stress symbol.
        ipaCharAlternatives.put(',', 'ˌ');

        // People may accidentally use a number three instead of the open-mid central unrounded vowel.
        ipaCharAlternatives.put('3', 'ɜ');
    }

    public static char getCanonical(char character) {
        if(ipaCharAlternatives.containsKey(character)) return ipaCharAlternatives.get(character);
        return character;
    }

    public static String getCanonical(String str) {
        return str.chars().map(IPAUtils::getCanonical)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }

    private static int getCanonical(int character) {
        return getCanonical((char) character);
    }
}
