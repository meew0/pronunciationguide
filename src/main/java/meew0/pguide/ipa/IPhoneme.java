package meew0.pguide.ipa;

/**
 * Created by meew0 on 17/07/18.
 */
public interface IPhoneme {
    public String getIPA();
    public String getDescription();

    public boolean isHighlightable();
}
