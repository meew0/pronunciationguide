package meew0.pguide;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;

/**
 * Created by meew0 on 18/07/18.
 */
public class ScaledFontRenderer extends FontRenderer {
    private final float fontSize;
    private boolean invis = false;

    public ScaledFontRenderer(GameSettings gameSettingsIn, ResourceLocation location, TextureManager textureManagerIn, boolean unicode, float fontSize) {
        super(gameSettingsIn, location, textureManagerIn, unicode);
        this.fontSize = fontSize;
    }

    // For determining the width of a string (for phoneme locations) without actually rendering it
    public void setInvis(boolean invis) {
        this.invis = invis;
    }

    protected float renderDefaultChar(int ch, boolean italic) {
        int i = ch % 16 * 8;
        int j = ch / 16 * 8;
        int k = italic ? 1 : 0;
        int l = this.charWidth[ch];
        float f = (float) l - 0.01F;
        if(!invis) {
            bindTexture(this.locationFontTexture);
            GlStateManager.glBegin(5);
            GlStateManager.scale(fontSize, fontSize, 1f);
            GlStateManager.glTexCoord2f((float) i / 128.0F, (float) j / 128.0F);
            GlStateManager.glVertex3f(this.posX + (float) k, this.posY, 0.0F);
            GlStateManager.glTexCoord2f((float) i / 128.0F, ((float) j + 7.99F) / 128.0F);
            GlStateManager.glVertex3f(this.posX - (float) k, this.posY + 7.99F, 0.0F);
            GlStateManager.glTexCoord2f(((float) i + f - 1.0F) / 128.0F, (float) j / 128.0F);
            GlStateManager.glVertex3f(this.posX + f - 1.0F + (float) k, this.posY, 0.0F);
            GlStateManager.glTexCoord2f(((float) i + f - 1.0F) / 128.0F, ((float) j + 7.99F) / 128.0F);
            GlStateManager.glVertex3f(this.posX + f - 1.0F - (float) k, this.posY + 7.99F, 0.0F);
            GlStateManager.glEnd();
        }
        return (float) l * 2;
    }

    protected float renderUnicodeChar(char ch, boolean italic) {
        int width = this.glyphWidth[ch] & 255;

        if (width == 0) {
            return 0.0F;
        } else {
            int indexOnTexture = ch / 256;
            int k = width >>> 4;
            int l = width & 15;
            float f = (float) k;
            float f1 = (float) (l + 1);
            float textureS = (float) (ch % 16 * 16) + f;
            float textureT = (float) ((ch & 255) / 16 * 16);
            float widthOnTexture = f1 - f - 0.02F;
            float italicModifier = italic ? 1.0F : 0.0F;
            if(!invis) {
                this.loadGlyphTexture(indexOnTexture);
                GlStateManager.glBegin(5);
                GlStateManager.glTexCoord2f(textureS / 256.0F, textureT / 256.0F);
                GlStateManager.glVertex3f(this.posX + italicModifier, this.posY, 0.0F);
                GlStateManager.glTexCoord2f(textureS / 256.0F, (textureT + 15.98F) / 256.0F);
                GlStateManager.glVertex3f(this.posX - italicModifier, this.posY + 7.99F * fontSize, 0.0F);
                GlStateManager.glTexCoord2f((textureS + widthOnTexture) / 256.0F, textureT / 256.0F);
                GlStateManager.glVertex3f(this.posX + widthOnTexture * fontSize / 2.0F + italicModifier, this.posY, 0.0F);
                GlStateManager.glTexCoord2f((textureS + widthOnTexture) / 256.0F, (textureT + 15.98F) / 256.0F);
                GlStateManager.glVertex3f(this.posX + widthOnTexture * fontSize / 2.0F - italicModifier, this.posY + 7.99F * fontSize, 0.0F);
                GlStateManager.glEnd();
            }
            return ((f1 - f) / 2.0F + 1.0F) * 2;
        }
    }
}
