package meew0.pguide;

import com.mojang.text2speech.Narrator;
import meew0.pguide.lexconvert.LexconvertCache;
import meew0.pguide.lib.TermRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.*;
import org.apache.logging.log4j.Logger;

import java.io.File;

@Mod(modid = PronunciationGuide.MODID, name = PronunciationGuide.NAME, version = PronunciationGuide.VERSION, clientSideOnly = true)
public class PronunciationGuide
{
    public static final String MODID = "pronunciationguide";
    public static final String NAME = "Pronunciation Guide";
    public static final String VERSION = "1.0";

    public static final TermRegistry TERM_REGISTRY = new TermRegistry();
    public static LexconvertCache lexconvertCache = null;

    public static boolean enableDebug;

    public static Logger logger;

    private ClientHandler keybindHandler;

    public static final Narrator narrator = Narrator.getNarrator();

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = event.getModLog();

        loadConfig(new File(event.getModConfigurationDirectory().getPath(), "pronunciationguide.cfg"));
        lexconvertCache = new LexconvertCache(new File(event.getModConfigurationDirectory().getPath(),
                "pronunciationguide_lexconvert_cache.json"));

        keybindHandler = new ClientHandler();
        MinecraftForge.EVENT_BUS.register(keybindHandler);
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        GuiPronunciationGuide.initialiseIPAFontRenderer();

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "elytra");
        FMLInterModComms.sendMessage("pronunciationguide", "associateRegistryItemWithTerm:elytra", Items.ELYTRA.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:elytra", new ItemStack(Items.ELYTRA));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "grass");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:grass", Blocks.GRASS.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:grass", new ItemStack(Blocks.GRASS));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:grass", new ItemStack(Blocks.GRASS));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "mycelium");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:mycelium", Blocks.MYCELIUM.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:mycelium", new ItemStack(Blocks.MYCELIUM));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:mycelium", new ItemStack(Blocks.MYCELIUM));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "diamond_ore");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:diamond_ore", Blocks.DIAMOND_ORE.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:diamond_ore", new ItemStack(Blocks.DIAMOND_ORE));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:diamond_ore", new ItemStack(Blocks.DIAMOND_ORE));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "coal_block");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:coal_block", Blocks.COAL_BLOCK.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:coal_block", new ItemStack(Blocks.COAL_BLOCK));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:coal_block", new ItemStack(Blocks.COAL_BLOCK));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "observer");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:observer", Blocks.OBSERVER.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:observer", new ItemStack(Blocks.OBSERVER));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:observer", new ItemStack(Blocks.OBSERVER));

        FMLInterModComms.sendMessage("pronunciationguide", "registerTerm", "brewing_stand");
        FMLInterModComms.sendMessage("pronunciationguide", "associateBlockWithTerm:brewing_stand", Blocks.BREWING_STAND.getRegistryName());
        FMLInterModComms.sendMessage("pronunciationguide", "associateUnlocalizedItemWithTerm:brewing_stand", new ItemStack(Items.BREWING_STAND));
        FMLInterModComms.sendMessage("pronunciationguide", "setDisplayItemStackForTerm:brewing_stand", new ItemStack(Items.BREWING_STAND));
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        TERM_REGISTRY.enableSorting();
    }

    @EventHandler
    public void onIMC(FMLInterModComms.IMCEvent event) {
        for(FMLInterModComms.IMCMessage message : event.getMessages()) {
            IMCHandler.handleIMCMessage(message);
        }
    }

    private void loadConfig(File file) {
        Configuration cfg = new Configuration(file);

        enableDebug = cfg.getBoolean("enable advanced tools", Configuration.CATEGORY_GENERAL, true,
                "Whether to enable advanced tools, like rebuilding the lexconvert cache. If true, a " +
                        "keybinding for each feature will be generated, as well as a modifier keybinding. Each " +
                        "feature can be triggered by pressing the relevant key as well as the modifier, while in the" +
                        "debug screen. If you are a pack dev, you may want to disable this option before releasing " +
                        "the pack");

        final String CATEGORY_LEXCONVERT = "lexconvert";
        cfg.addCustomCategoryComment(CATEGORY_LEXCONVERT, "Options for running lexconvert. This is mostly " +
                "useful if you are a pack developer and want to enable speech synthesis for all terms. See " +
                "<URL> for more information"); // TODO: add URL

        LexconvertCache.pythonExecutablePath = cfg.getString(
                "pythonExecutablePath",
                CATEGORY_LEXCONVERT,
                "",
                "The path to a valid Python 2 (!) executable"
        );

        LexconvertCache.lexconvertPath = cfg.getString(
                "lexconvertPath",
                CATEGORY_LEXCONVERT,
                "",
                "The path to a copy of lexconvert.py (get it here: " +
                        "http://people.ds.cam.ac.uk/ssb22/gradint/lexconvert.html)"
        );

        LexconvertCache.windowsFormat = cfg.getString(
                "windowsFormat",
                CATEGORY_LEXCONVERT,
                "sapi",
                "The output format to use for speech synthesis on Windows. This and the other xyzFormat " +
                        "options should not be changed unless you know what you're doing, otherwise synthesised " +
                        "speech will sound completely wrong"
        );

        LexconvertCache.macFormat = cfg.getString(
                "macFormat",
                CATEGORY_LEXCONVERT,
                "mac",
                "The output format to use for speech synthesis on Mac."
        );

        LexconvertCache.linuxFormat = cfg.getString(
                "linuxFormat",
                CATEGORY_LEXCONVERT,
                "festival",
                "The output format to use for speech synthesis on Linux."
        );

        if(cfg.hasChanged()) cfg.save();
    }
}
